# Test Demo

This is an sample Java project that demonstrates common unit testing scenarios using [JUnit](http://junit.org/junit4/), [Mockito](http://site.mockito.org/) and
[PowerMock](http://powermock.github.io/).  The is a maven based project that can be imported into either Eclipse or IntelliJ IDEA.

## [SimpleService1Tests](https://github.com/roundwheel/testdemo/blob/master/src/test/java/ca/roundwheel/service/SimpleService1Test.java)

This is the simplest case, where we are testing [a class that has no external dependencies](https://github.com/roundwheel/testdemo/blob/master/src/main/java/ca/roundwheel/service/SimpleService1.java).

## [SimpleService2Tests](https://github.com/roundwheel/testdemo/blob/master/src/test/java/ca/roundwheel/service/SimpleService2Test.java)

Here [we introduce an external dependency](https://github.com/roundwheel/testdemo/blob/master/src/main/java/ca/roundwheel/service/SimpleService2.java) in the form of an injected EJB which we can easily mock using Mockito.  We introduce the _@Mock_ and _@InjectMocks_ annotations, expectations using _Mockito.when()_ and assertions using _Mockito.verify()_.

## [SimpleService3Tests](https://github.com/roundwheel/testdemo/blob/master/src/test/java/ca/roundwheel/service/SimpleService3Test.java)

Here we look at a scenario where [the class we are testing modifies our input](https://github.com/roundwheel/testdemo/blob/master/src/main/java/ca/roundwheel/service/SimpleService3.java) before handing it off to its dependency.  We introduce the _@Captor_ annotation and the _capture()_ method.

## [SimpleService4Tests](https://github.com/roundwheel/testdemo/blob/master/src/test/java/ca/roundwheel/service/SimpleService4Test.java)

Here we look at how to test [a class that has dependencies on static methods](https://github.com/roundwheel/testdemo/blob/master/src/main/java/ca/roundwheel/service/SimpleService4.java) using _PowerMockito.mockStatic()_ and _PowerMockito.verifyStatic()_.