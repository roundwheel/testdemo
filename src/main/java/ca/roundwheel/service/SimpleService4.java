package ca.roundwheel.service;

import javax.ejb.Local;
import javax.ejb.Stateless;

import ca.roundwheel.service.foo.FooUtils;

/**
 * {@code SimpleService2} is a facade with an external dependency on a static method.
 *
 * @author Yvon X. Comeau
 */
@Local
@Stateless(name = "SimpleService3", mappedName="SimpleService3")
public class SimpleService4 {
	
	/**
	 * Returns something.
	 * 
	 * @return foo
	 */
	public String getSomething() {
		return FooUtils.determineFooStatic();
	}
	
	/**
	 * Returns something.
	 * 
	 * @return foo
	 */
	public String getSomethingWithInput() {
		return FooUtils.determineFooStatic("foo");
	}
}
