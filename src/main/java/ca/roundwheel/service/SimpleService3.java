package ca.roundwheel.service;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import ca.roundwheel.domain.Foo;
import ca.roundwheel.service.foo.FooCountingService;

/**
 * {@code SimpleService3} is a facade with an external dependency on an injected EJB.
 *
 * @author Yvon X. Comeau
 */
@Local
@Stateless(name = "SimpleService3", mappedName="SimpleService3")
public class SimpleService3 {

	@EJB 
	private FooCountingService fooCountingService;
	
	/**
	 * Increments foo's count.  
	 * 
	 * The point of this method and test is simply to highlight the fact that we control both the 
	 * test input, and the mocked output.  We don't care about the implementation of the 
	 * {@code incrementFoo} method.  Whatever we specify as our expectation's return object is what 
	 * we will receive as the response, and assert.
	 * 
	 * @param foo
	 * @return foo, incremented
	 */
	public Foo countFoo(Foo foo) {
		return fooCountingService.incrementFoo(foo);
	}
	
	/**
	 * Set the counter value to 5 and then increment it.
	 * 
	 * The point of this method is to show that our unit test should focus checking the modified
	 * value of the {@code foo} that is passed as an input, rather than the response, since the
	 * {@code foo.setCounter(5);} is the logic that we are testing, and not the behaviour of
	 * {@code fooCountingService.incrementFoo(foo)}.
	 * 
	 * 
	 * @param foo
	 * @return foo, incremented twice
	 */
	public Foo modifyFooCount(Foo foo) {
		// Increments before sending it to the service..
		foo.setCounter(5);
		// Sends it to the service...
		return fooCountingService.incrementFoo(foo);
	}
}
