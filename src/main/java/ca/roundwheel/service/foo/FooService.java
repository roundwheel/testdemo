package ca.roundwheel.service.foo;

import javax.ejb.Local;
import javax.ejb.Stateless;

/**
 * {@code FooService} does foo...
 * 
 * @author Yvon X Comeau
 */
@Local
@Stateless(name = "FooService", mappedName="FooService")
public class FooService {
	
	/**
	 * Returns foo.
	 * 
	 * @return foo
	 */
	public String determineFoo() {
		return "foo";
	}

	/**
	 * Returns foo for the given bar.
	 * 
	 * @param bar 
	 * @return foo
	 */
	public String determineFoo(String bar) {
		return "foo";
	}
}