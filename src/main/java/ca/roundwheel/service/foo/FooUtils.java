package ca.roundwheel.service.foo;

/**
 * {@code FooUtils} sucks.
 * 
 * @author Yvon X Comeau
 */
public class FooUtils {

	/**
	 * A static method that returns bar.
	 * 
	 * @return bar
	 */
	public static String determineFooStatic() {
		return "bar";
	}

	/**
	 * A static method with an input that returns bar.
	 * 
	 * @return bar
	 */
	public static String determineFooStatic(String input) {
		return "bar";
	}
}
