package ca.roundwheel.service.foo;

import javax.ejb.Local;
import javax.ejb.Stateless;

import ca.roundwheel.domain.Foo;

/**
 * {@code FooCountingService} handles foo counting.
 * 
 * @author Yvon X Comeau
 */
@Local
@Stateless(name = "FooCountingService", mappedName="FooCountingService")
public class FooCountingService {

	/**
	 * Increments foo's count by one.
	 * 
	 * @param foo
	 * 
	 * @return
	 */
	public Foo incrementFoo(Foo foo) {
		foo.increment();
		return foo;
	}
}
