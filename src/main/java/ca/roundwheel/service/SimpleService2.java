package ca.roundwheel.service;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import ca.roundwheel.service.foo.FooService;

/**
 * {@code SimpleService2} is a facade with an external dependency on an injected EJB.
 *
 * @author Yvon X. Comeau
 */
@Local
@Stateless(name = "SimpleService2", mappedName="SimpleService2")
public class SimpleService2 {

	@EJB 
	private FooService fooService;
	
	/**
	 * Returns something by calling the foo service.
	 * 
	 * @return foo
	 */
	public String getSomething() {
		return fooService.determineFoo();
	}
	
	/**
	 * Returns something.
	 * 
	 * @param bar
	 * @return foo
	 */
	public String getSomething(String bar) {
		return fooService.determineFoo(bar);
	}
	
	/**
	 * Returns something, checking the input for nulls.
	 * 
	 * @param bar	 
	 * @return foo
	 */
	public String getSomethingVaildateInput(String bar) {
		if (bar == null) {
			return "";
		} 
		
		return fooService.determineFoo(bar);
	}
	
	/**
	 * Returns something, checking the result for nulls.
	 * 
	 * @param bar	 
	 * @return foo
	 */
	public String getSomethingMoreComplex(String bar) {
		String foo = fooService.determineFoo(bar);
		
		if (foo == null || foo.equalsIgnoreCase(" ")) {
			return "";
		} 
		
		return foo;
	}
	
	/**
	 * Returns something.
	 * 
	 * @param bar	 
	 * @return foo
	 */
	public String getSomethingMoreComplexWithError(String bar) {
		String foo = fooService.determineFoo(bar);
		
		if (foo == null) {
			throw new NullPointerException("WFT");
		} 
		
		return foo;
	}
	
	/**
	 * Returns something.
	 * 
	 * @param bar	 
	 * @return foo
	 * 
	 * @throws IOException 
	 */
	public String getSomethingMoreComplexWithException(String bar) throws IOException {
		String foo = fooService.determineFoo(bar);
		
		if (foo == null) {
			throw new IOException("WFT");
		} 
		
		return foo;
	}
}
