package ca.roundwheel.service;

/**
 * {@code SimpleService} is a very basic facade with no external dependencies...
 *
 * @author Yvon X. Comeau
 */
public class SimpleService1 {

	/**
	 * Returns something.
	 * 
	 * @return foo
	 */
	public String getSomething() {
		return "foo";
	}
}
