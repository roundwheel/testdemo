package ca.roundwheel.domain;

/**
 * {@code Foo} contains data...
 * 
 * @author Yvon X Comeau
 */
public class Foo {

	/**
	 * The ID...
	 */
	private final long id;
	
	/**
	 * A name...
	 */
	private final String name;
	
	/**
	 * A counter...
	 */
	private int counter = 0;
	
	/**
	 * Creates a new instance of a {@code Foo} object.
	 * 
	 * @param id
	 * @param name
	 */
	public Foo(long id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Increments the count by one..
	 */
	public void increment() {
		counter++;
	}
	
	/*
	 * Getters and Setters...
	 */
	
	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
}
