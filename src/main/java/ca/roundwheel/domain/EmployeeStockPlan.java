package ca.roundwheel.domain;

class Employee {
	double b;
}

class Manager extends Employee {
}

public class EmployeeStockPlan {
	
	static Employee employee;

	public void grantStock() {
		if (employee instanceof Manager) {
			employee.b = 80000;
		} else {
			employee.b = 50000;
		}
	}

	public String toString() {
		return ((Employee) employee).b + " ";
	}

	public static void main(String[] args) {
		employee = new Employee();
		EmployeeStockPlan stockPlan = new EmployeeStockPlan();
		stockPlan.grantStock();
		System.out.println(stockPlan);
	}
}