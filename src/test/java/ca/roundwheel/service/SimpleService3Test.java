package ca.roundwheel.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import ca.roundwheel.domain.Foo;
import ca.roundwheel.service.SimpleService3;
import ca.roundwheel.service.foo.FooCountingService;

/**
 * {@code SimpleServiceTests} tests SimpleService3...
 * 
 * @author Yvon X Comeau
 */
@RunWith(MockitoJUnitRunner.class)
public class SimpleService3Test {

	@Mock
	private FooCountingService mockFooCountingService;

	@InjectMocks
	private SimpleService3 simpleService3;

	@Captor
	private ArgumentCaptor<Foo> fooCaptor;
	
	/*
	 * incrementFoo()
	 */
	@Test
	public void testIncrementFooAnyFoo() {
		
		// Setup the inputs/outputs...
		Foo myInput = new Foo(1000L, "inputFoo");
		Foo myMockResult = new Foo(1015L, "resultFoo");
		
		// Setup the expectations...
		Mockito.when(mockFooCountingService.incrementFoo(Mockito.any(Foo.class))).thenReturn(myMockResult);

		// Run the test...
		Foo result = simpleService3.countFoo(myInput);

		// Verify that the result looks as expected
		Mockito.verify(mockFooCountingService, Mockito.times(1)).incrementFoo(Mockito.any(Foo.class));
		assertEquals(1015L, result.getId());
		assertEquals("resultFoo", result.getName());
		assertEquals(0, result.getCounter());
	}
	
	@Test
	public void testIncrementFooSpecificFoo() {
		
		// Setup the inputs/outputs...
		Foo myInput = new Foo(1000L, "inputFoo");
		Foo myMockResult = new Foo(1015L, "resultFoo");

		// Setup the expectations...
		Mockito.when(mockFooCountingService.incrementFoo(myInput)).thenReturn(myMockResult);

		// Run the test...
		Foo result = simpleService3.countFoo(myInput);

		// Verify that the result looks as expected
		Mockito.verify(mockFooCountingService, Mockito.times(1)).incrementFoo(myInput);
		assertEquals(1015L, result.getId());
		assertEquals("resultFoo", result.getName());
		assertEquals(0, result.getCounter());
	}
	
	@Test
	public void testIncrementFooIncrementResult() {
		
		// Setup the inputs/outputs...
		Foo myInput = new Foo(1000L, "inputFoo");
		Foo myMockResult = new Foo(1015L, "resultFoo");
		myMockResult.increment();
		myMockResult.increment();
		myMockResult.increment();
		myMockResult.increment();
		
		// Setup the expectations...
		Mockito.when(mockFooCountingService.incrementFoo(myInput)).thenReturn(myMockResult);

		// Run the test...
		Foo result = simpleService3.countFoo(myInput);

		// Verify that the result looks as expected
		Mockito.verify(mockFooCountingService, Mockito.times(1)).incrementFoo(myInput);
		assertEquals(1015L, result.getId());
		assertEquals("resultFoo", result.getName());
		assertEquals(4, result.getCounter());
	}
	
	/*
	 * modifyFooCount()
	 */
	@Test
	public void testModifyFooCount() {
		
		// Setup the inputs/outputs...
		Foo myInput = new Foo(1000L, "inputFoo");
		Foo myMockResult = new Foo(1015L, "resultFoo");
		
		// Setup the expectations...
		Mockito.when(mockFooCountingService.incrementFoo(myInput)).thenReturn(myMockResult);
		
		// Run the test...
		Foo result = simpleService3.modifyFooCount(myInput);
		
		// Verify that the Foo sent to the service was incremented
		Mockito.verify(mockFooCountingService).incrementFoo(fooCaptor.capture());
		
		// Verify that the captured Foo looks as expected
		Foo interceptedFoo = fooCaptor.getValue();
		assertEquals(1000L, interceptedFoo.getId());
		assertEquals("inputFoo", interceptedFoo.getName());
		assertEquals(5, interceptedFoo.getCounter());
		
		// Verify that the result looks as expected
		assertEquals(1015L, result.getId());
		assertEquals("resultFoo", result.getName());
		assertEquals(0, result.getCounter());
	}
}