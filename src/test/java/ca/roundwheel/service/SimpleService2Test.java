package ca.roundwheel.service;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import ca.roundwheel.service.foo.FooService;

/**
 * {@code SimpleServiceTests} tests SimpleService2...
 * 
 * @author Yvon X Comeau
 */
@RunWith(MockitoJUnitRunner.class)
public class SimpleService2Test {

	@Mock
	private FooService mockFooService;

	@InjectMocks
	private SimpleService2 simpleService2;
	
//	@Before
//	public void init() {
//		simpleService2 = new SimpleService2();
//		simpleService2.setFooService(mockFooService);
//	}
	
	/*
	 * getSomething() - calls a service method with no parameters
	 */
	@Test
	public void testGetSomething() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo()).thenReturn("foo");
		
		// Run the test...
		String result = simpleService2.getSomething();
		
		// Verify that the result looks as expected
		assertEquals("foo", result);
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo();
	}
	
	/*
	 * getSomething(String bar)  - calls a service method with a parameter
	 */
	@Test
	public void testGetSomethingWithBar() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn("foo");
		
		// Run the test...
		String result = simpleService2.getSomething("mytest");
				
		// Verify that the result looks as expected
		assertEquals("foo", result);
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo(Mockito.anyString());
	}
	
	/*
	 * getSomethingVaildateInput(String bar)  - calls a service method, checking the input 
	 * parameter.
	 */
	@Test
	public void testGetSomethingVaildateInputNullInput() {
		
		// Run the test...
		String result = simpleService2.getSomethingVaildateInput(null);
				
		// Verify that the result looks as expected
		assertEquals("", result);
		Mockito.verify(mockFooService, Mockito.times(0)).determineFoo(Mockito.anyString());
	}
	
	@Test
	public void testGetSomethingVaildateInputValidInput() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn("foo");
		
		// Run the test...
		String result = simpleService2.getSomethingVaildateInput("mytest");
		
		// Verify that the result looks as expected
		assertEquals("foo", result);
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo(Mockito.anyString());
	}
	
	/*
	 * getSomethingMoreComplex(String bar)  - calls a service method with a parameter
	 * and then acts on the response.
	 */
	@Test
	public void testGetSomethingMoreComplexNullResponse() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn(null);
		
		// Run the test...
		String result = simpleService2.getSomethingMoreComplex("mytest");
				
		// Verify that the result looks as expected
		assertEquals("", result);
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo(Mockito.anyString());
	}
	
	@Test
	public void testGetSomethingMoreComplexNonNullResponse2() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn("foo");
		
		// Run the test...
		String result = simpleService2.getSomethingMoreComplex("mytest");
				
		// Verify that the result looks as expected
		assertEquals("foo", result);
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo(Mockito.anyString());
	}
	
	@Test
	public void testGetSomethingMoreComplexNonNullResponse() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn("foo");
		
		// Run the test...
		String result = simpleService2.getSomethingMoreComplex("mytest");
				
		// Verify that the result looks as expected
		assertEquals("foo", result);
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo(Mockito.anyString());
	}
	
	/*
	 * getSomethingMoreComplexWithError(String bar)  - calls a service method with a parameter
	 * and then acts on the response, throwing a runtime exception if the result is null.
	 */
	@Test(expected=NullPointerException.class)
	public void testgGetSomethingMoreComplexWithErrorNullResponse() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn(null);
		
		// Run the test...
		simpleService2.getSomethingMoreComplexWithError("mytest");
	}
	
	@Test
	public void testGetSomethingMoreComplexWithErrorNonNullResponse() {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn("foo");
		
		// Run the test...
		String result = simpleService2.getSomethingMoreComplexWithError("mytest");
				
		// Verify that the result looks as expected
		assertEquals("foo", result);
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo(Mockito.anyString());
	}
	
	/*
	 * getSomethingMoreComplexWithError(String bar)  - calls a service method with a parameter
	 * and then acts on the response, throwing an exception if the result is null.
	 */
	@Test(expected=IOException.class)
	public void testGetSomethingMoreComplexWithExceptionNullResponse() throws IOException {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn(null);
		
		// Run the test...
		simpleService2.getSomethingMoreComplexWithException("mytest");
	}
	
	@Test
	public void testGetSomethingMoreComplexWithExceptionNonNullResponse() throws IOException {
		
		// Setup the expectations...
		Mockito.when(mockFooService.determineFoo(Mockito.anyString())).thenReturn("foo");
		
		// Run the test...
		String result = simpleService2.getSomethingMoreComplexWithException("mytest");
				
		// Verify that the result looks as expected
		assertEquals("foo", result);		
		Mockito.verify(mockFooService, Mockito.times(1)).determineFoo(Mockito.anyString());
	}
}