package ca.roundwheel.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ca.roundwheel.service.SimpleService1;

/**
 * {@code SimpleServiceTests} tests SimpleService...
 * 
 * @author Yvon X Comeau
 */
public class SimpleService1Test {

	private SimpleService1 simpleService;
	
	@Before
	public void init() {
		// Initialize the service we want to test...
		simpleService = new SimpleService1();
	}
	
	@Test
	public void testGetSomething() {
		
		SimpleService1 simpleService = new SimpleService1();
		
		// Run the test...
		String result = simpleService.getSomething();
		
		// Verify that the result looks as expected
		assertEquals("foo", result);
	}
}