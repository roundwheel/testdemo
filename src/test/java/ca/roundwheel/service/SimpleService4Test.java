package ca.roundwheel.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ca.roundwheel.service.SimpleService4;
import ca.roundwheel.service.foo.FooUtils;

/**
 * {@code SimpleServiceTests} tests SimpleService4...
 * 
 * @author Yvon X Comeau
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({FooUtils.class})
public class SimpleService4Test {

	@InjectMocks
	private SimpleService4 simpleService4;
	
	/*
	 * getSomething() - calls a service method that calls a static
	 */
	@Test
	public void testGetSomething() {

		// Setup the expectations...
		PowerMockito.mockStatic(FooUtils.class);
		PowerMockito.when(FooUtils.determineFooStatic()).thenReturn("bar");

		// Run the test...
		String result = simpleService4.getSomething();

		// Verify that the result looks as expected
		assertEquals("bar", result);

		PowerMockito.verifyStatic(Mockito.times(1)); 
		FooUtils.determineFooStatic();
	}
	
	/*
	 * getSomething() - calls a service method that calls a static
	 */
	@Test
	public void getSomethingWithInput() {

		// Setup the expectations...
		PowerMockito.mockStatic(FooUtils.class);
		PowerMockito.when(FooUtils.determineFooStatic(Mockito.anyString())).thenReturn("bar");

		// Run the test...
		String result = simpleService4.getSomethingWithInput();

		// Verify that the result looks as expected
		assertEquals("bar", result);
		
		PowerMockito.verifyStatic(Mockito.times(1)); 
		FooUtils.determineFooStatic(Mockito.anyString());
	}
}