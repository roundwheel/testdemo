import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * {@code AllTests} runs all unit tests
 * 
 * @author Yvon X Comeau
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	ca.roundwheel.service.SimpleService1Test.class,
	ca.roundwheel.service.SimpleService2Test.class,
	ca.roundwheel.service.SimpleService3Test.class,
	ca.roundwheel.service.SimpleService4Test.class
})
public class AllTests {

}